/** @type {import('vite').UserConfig} */
import basicSsl from '@vitejs/plugin-basic-ssl'

export default {
    base: '/boolean-game',
    server: {
        https: true
    },
    build: {
        target: 'esnext' //browsers can handle the latest ES features
    },
    plugins: [
        basicSsl()
    ]
}