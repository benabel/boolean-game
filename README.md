# Boolean game

> Un jeu pour apprendre l'algèbre booléen avec vos élèves.

Page généreusement hébergée sur framagit à l'adresse:
https://benabel.frama.io/boolean-game/.

## Développement

Le projet est construit avec vite et utilise pnpm pour la gestion des paquets.

Commencer par installer les dépendences.

```
pnpm install
```

Lancer le serveur de développement.

```
pnpm dev
```

## Structure du projet

─ `levels.ts`: ce fichier permet de définir les niveaux du jeu en les chargeant à partir des fichiers `emd` ou en générant un circuit booléen aléatoire pour rendre le jeu infini.
─ `main.ts`: c'est le fichier principal du jeu, il permet de générer le code html, lancer le jeu et de gérer les évènements.
─ `_nd-switch.css`: ce fichier provient de la librairie open-source [nudeui](nudeui.com) et permet de styliser les switchs.
─ `scores.ts`: ce fichier permet de stocker les scores dans une base de données via une API (WIP).
─ `sim.ts`: ce fichier permet de gérer la simulation du circuit booléen.
─ `start.ts`: ce fichier permet d'initialiser le jeu.
─ `state.ts`: ce fichier permet la gestion de l'état du jeu via un objet global `state`.
─ `style.css`: fichier de style faisant appel à la librairie [open-props](https://open-props.style/).
─ `ui.ts`: ce fichier permet de gérer l'interface utilisateur.

## Licence

Ce projet est sous licence MIT. Voir le fichier [LICENSE](LICENSE) pour plus de details.
