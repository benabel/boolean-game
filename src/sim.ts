import { operand, operator, link, upDateLinks } from "./ui.js";
export function createSim(state) {
  console.log("Creating sim");
  // add one operator initially
  const simBlocks = document.querySelector<HTMLElement>("div.blocks");
  simBlocks?.append(operand(state));
  simBlocks?.append(link());

  // add and remove operators
  const addButton = document.querySelector("#add-op");
  const rmButton = document.querySelector("#rm-op");

  // allow to add or remove operators with
  addButton.addEventListener("click", () => {
    console.log("add operator");
    simBlocks?.append(operator(state));
    simBlocks?.append(operand(state));
    simBlocks?.append(link());
    upDateLinks(state);
    // show remove operator button
    rmButton?.classList.remove("hidden");
  });

  document.querySelector("#rm-op")?.addEventListener("click", (evt) => {
    // Don't remove the first!
    if (state.getState().ops.length > 2) {
      console.log("remove operator");
      // this need 3 repetitions
      Array(3)
        .fill(null)
        .forEach(() => {
          // remove block
          simBlocks?.removeChild(simBlocks.lastChild);
          // update state.ops
          state.popOps();
        });
      // hide button if only 1 operand left
      if (state.getState().ops.length < 3) {
        rmButton.classList.add("hidden");
      }
    }
  });
}
