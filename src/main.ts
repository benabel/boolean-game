import "./style.css";
import registerElements from "@ed-components/ed-components";

import State from "./state.js";
import { updateLevel } from "./levels.js";
import { renderScores } from "./scores.js";

registerElements();

document.querySelector<HTMLDivElement>("#app")!.innerHTML = `
<div>
    <header>
        <div class="level"></div>
        <div class="score"></div>
    </header>
    <dialog>
        <header>
        <h3>Boolean Game</h3>
        <button id="close-dialog" onclick="this.closest('dialog').close('close');document.querySelector('.info').scrollIntoView({behavior: 'smooth'});" type="button" title="Close dialog"> 
            <title>Close dialog icon</title>
            <svg width="24" height="24" viewBox="0 0 24 24">
              <line x1="18" y1="6" x2="6" y2="18"/>
              <line x1="6" y1="6" x2="18" y2="18"/>
            </svg>
        </button>
        </header>
        <main>
        </main>
        <footer>
            <div class="level"></div>
            <div class="score"></div>
        </footer>
    </dialog>
    <main>
        <section class="title">
            <h1>The boolean game</h1>
        </section>
        <section class="info">
            <h2>Info</h2>
            <section class="current-info">
            </section>
        </section>
        <section class="sim">
            <h2>Simulation</h2>
            <section class="current-sim">
                <div class="blocks"></div>
                <div class="modify-blocks">
                    <button id="add-op">Add operator</button>
                    <button id="rm-op" class="hidden">Remove operator</button>
                </div>
            </section>
        </section>
        <section class="game">
            <h2>Game</h2>
            <section class="current-game">
            </section>
        </section>
        <section class="scores">
            <h2>Best scores</h2>
        </section>
    </main>
</div>
`;

// Init state
let state: State = new State({ ops: [], level: 0, score: 0 });

await updateLevel(state);

await renderScores();
