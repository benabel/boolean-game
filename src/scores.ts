// fetch scores from http://apitest.lyceum.fr:8000/scores/ and display them in a table

const fetchScores = async () => {
  const response = await fetch("http://apitest.lyceum.fr:8000/scores/");
  if (!response.ok) {
    throw new Error("HTTP error " + response.status);
  }
  return response.json();
};

export async function renderScores() {
  const scoreSection = document.querySelector(".scores");
  let scores;
  try {
    scores = await fetchScores();
  } catch (error) {
    // TypeError: Failed to fetch
    console.error("There was an error", error);
    scores = [];
  }
  if (scores.length > 0) {
    // create table element
    const table = document.createElement("table");
    // create table header
    const thead = document.createElement("thead");
    const headerRow = thead.insertRow();
    // use first elemnt of scores to create header
    Object.keys(scores[0]).forEach((key) => {
      headerRow.insertCell().innerHTML = key;
    });
    // append header to table
    table.appendChild(thead);
    // create table body
    const tbody = document.createElement("tbody");
    // iterate over scores to create rows
    scores.forEach((score) => {
      const row = tbody.insertRow();
      // iterate over score to create cells
      Object.values(score).forEach((value) => {
        row.insertCell().innerHTML = value.toString();
      });
    });
    // append body to table
    table.appendChild(tbody);
    // append table to section
    scoreSection.appendChild(table);
  } else {
    scoreSection?.parentNode?.removeChild(scoreSection);
  }
}

renderScores();
