// state object will store
// boolean operations under the ops array
export default class State {
  constructor(public state: object) {
    this.state = state ? state : this.resetState();
  }

  getState() {
    return this.state;
  }

  setState(newState: object) {
    this.state = newState;
  }

  resetState() {
    this.state = { ops: [], level: 0, score: 0 };
  }

  getLevel() {
    return this.state.level;
  }

  setLevel(level: bigint) {
    this.state.level = level;
  }

  getScore() {
    return this.state.score;
  }

  setScore(score: bigint) {
    this.state.score = score;
  }

  getOps() {
    return this.state.ops;
  }

  pushOps(newOps: array) {
    this.state.ops = this.state.ops.concat(newOps);
  }

  popOps() {
    return this.state.ops.pop();
  }

  // set op at index i
  setOp(op, i) {
    console.log("set", op, "at", i);
    this.state.ops[i] = op;
  }

  /**
   * Evaluate result at index i
   *
   * @param {*} i
   * @memberof State
   */
  evalRes(i: BigInt) {
    // first operand contain 2 ops
    // and others 3
    if ((i - 2) % 3 !== 0) {
      throw new Error(`Can't evaluate at ${i}`);
    }
    // Create a string from ops
    const opsString = this.state.ops.slice(0, i).reduce((acc, val) => {
      return acc + val;
    });
    console.log(opsString, eval(opsString));

    // eval string as boolean
    return eval(opsString);
  }
}
