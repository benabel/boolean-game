import { md2HTML } from "@ed-components/ed-components/common.js";
import confetti from "canvas-confetti";
import { createSim } from "./sim";
import { startGame } from "./start";

export async function updateLevel(state) {
  // update scores
  const score = state.getScore();
  document
    .querySelectorAll(".score")
    .forEach((el) => (el.innerHTML = `Score ${score}`));

  // handle levels
  const level = state.getLevel();
  document
    .querySelectorAll(".level")
    .forEach((el) => (el.innerHTML = `Level ${level}`));

  if (level === 0) {
    await startGame(state);

    // hide game and sim at level 0
    document.querySelector(".game")?.classList.add("hidden");
    document.querySelector(".sim")?.classList.add("hidden");
  } else if (level < 7) {
    showDialog();

    // normal game with provided questions in emd format
    await fillInfo(level);

    // Add question in game section as an ed-pb
    const mdExReq = await fetch(`./levels/level${level}-q.emd`);
    const mdExContent = await mdExReq.text();
    document.querySelector(".current-game")!.innerHTML = `<ed-pb>
  ${mdExContent}
  </ed-pb>
`;
  } else {
    // infinite game
    console.log("Infinite game starts");
    // show modal and confetties
    showDialog();

    // we add one operation every 3 levels
    const nOps = Math.floor(level / 3);
    const operation = new Array(1 + 2 * nOps)
      .fill(0)
      .map((_, i) => (i % 2 === 0 ? randomOperator() : randomOperand()));
    const opString: string = operation
      .map((l) => (l === "&" ? "\\And" : l))
      .join(" ");
    const result: bigint = BigInt(
      eval(
        operation.reduce((acc, val) => {
          return acc + val;
        })
      )
    );
    document.querySelector(".current-info")!.innerHTML =
      md2HTML(`### Evaluate this boolean operation
    
$$
${opString}
$$
    `);

    // add question to game
    document.querySelector(".current-game")!.innerHTML = `<ed-pb>
1. What is the result of this operation ?

   $$
   ${opString}
   $$

   - :num: ${result}

</ed-pb>`;
  }
  if (level === 1) {
    // show questions
    document.querySelector(".game")?.classList.remove("hidden");
  }
  // Since level 2 add simulation for helping purposes
  if (level === 2) {
    console.log("Show simulation");
    createSim(state);
    document.querySelector(".sim")?.classList.remove("hidden");
    // Hide add operator at the moment
    document.querySelector("#add-op")?.classList.add("hidden");
  }
  // Since level 3 we can use one or more operator
  if (level === 3) {
    console.log("Allow adding operator");
    // Hide add operator at the moment
    document.querySelector("#add-op")?.classList.remove("hidden");
  }
}

// Helpers

// fetch emd content

export async function fillInfo(level: bigint) {
  // Add info based on level
  const mdReq = await fetch(`./levels/level${level}.emd`);
  const mdContent = await mdReq.text();
  document.querySelector(".current-info")!.innerHTML = md2HTML(mdContent);
}

function showDialog() {
  // show modal and confetties
  const dialog = document.querySelector("dialog");
  // dialog.querySelector
  dialog!.showModal();
  confetti({ scalar: 3 });
}
// Operators
/**
 *Get random element from array
 *
 * @param {array} array
 * @return {*}
 */
function getRandom(array: array) {
  return array[Math.floor(Math.random() * array.length)];
}

const randomOperator = () => getRandom(["true", "false", "!true", "!false"]);

const randomOperand = () => getRandom(["&", "|"]);
