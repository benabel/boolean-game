import State from "./state.js";

export function operator(state: State) {
  // initialize with &&
  state.pushOps(["&&"]);

  // get number of operations to allow update on click later
  const nOp = state.getState().ops.length - 1;
  const div = document.createElement("div");
  div.className = "block";
  div.innerHTML = `<div class="type">operator</div>
  <div class="operator and active" data-nop="${nOp}">&</div>
    <div class="operator or">|</div>
    <div class="value">and</div>`;
  // Toggle active class on click
  div.addEventListener("click", (evt) => {
    div.querySelectorAll(".operator").forEach((el) => {
      el.classList.toggle("active");
      // update ops on state
      const op = evt.target.classList.contains("active") ? "&&" : "||";
      // update value displayed
      div.querySelector(".value").innerHTML = op == "&&" ? "AND" : "OR";
      // update state
      state.setOp(op, nOp);

      // update value displayed
      div.querySelector(".value").innerHTML = state
        .getOps()
        .slice(nOp, nOp + 1)
        .join(" ")
        .replace("&&", "AND")
        .replace("||", "OR");
      upDateLinks(state);
    });
  });

  return div;
}

export function operand(state: State) {
  // initialize at true
  state.pushOps(["", "true"]);
  // get number of operations to allow update on click later
  const nOp = state.getState().ops.length;
  const div = document.createElement("div");
  div.className = "block";
  div.innerHTML = `<div class="type">operand</div>
  <div class="operator first" data-nop="${nOp - 2}">!</div>
  <div class="second"><input type="checkbox" class="on-off nd-switch" checked /></div>
  <div class="value">true</div>
    `;
  // event listener to toggle active class
  div.querySelector(".operator").addEventListener("click", (evt) => {
    evt.target.classList.toggle("active");
    // update ops on state
    const op = evt.target.classList.contains("active") ? "!" : "";
    state.setOp(op, nOp - 2);

    // update value displayed
    div.querySelector(".value").innerHTML = state
      .getOps()
      .slice(nOp - 2, nOp)
      .join(" ")
      .replace("!", "NOT");

    // update state
    upDateLinks(state);
  });
  div.querySelector(".on-off").addEventListener("change", (evt) => {
    console.log("SWITCH");
    // evt.target.classList.toggle("active");
    // some strange hack I would i thought on => true
    const op = evt.target.checked ? "true" : "false";
    state.setOp(op, nOp - 1);

    // update value displayed
    div.querySelector(".value").innerHTML = state
      .getOps()
      .slice(nOp - 2, nOp)
      .join(" ")
      .replace("!", "NOT");
    upDateLinks(state);
  });

  return div;
}

export function link() {
  const div = document.createElement("div");
  div.className = "link";
  const wire = document.createElement("div");
  wire.className = "wire";
  wire.innerHTML = "1";
  div.append(wire);
  return div;
}

/**
 * Update link values based on state.ops
 *
 * @export
 * @param {*} state
 */
export function upDateLinks(state: any) {
  // console.log("Update links");
  document
    .querySelectorAll(".wire")
    .forEach((el: HTMLDivElement, i: BigInt) => {
      el.innerHTML = String(state.evalRes(2 + 3 * i) ? 1 : 0);
    });
}
