import { md2HTML } from "@ed-components/ed-components/common.js";
import { updateLevel } from "./levels";
import { fillInfo } from "./levels";

export async function startGame(state) {
  // welcome message
  document.querySelector("dialog > main")!.innerHTML = md2HTML(
    "_Welcome_ to the **boolean Game**."
  );

  // show close dialog button
  document.querySelector("#close-dialog")?.classList.remove("hidden");

  // retrieve info
  await fillInfo(state.getLevel());

  // Add button to start game
  document.querySelector("#start-game").addEventListener("click", async () => {
    let newState = state.getState();
    // console.log(newState);
    newState.level = 1;
    state.setState(newState);
    await updateLevel(state);
  });

  // listen to completed events to advance levels
  document.querySelector("body")?.addEventListener("edEvent", (evt) => {
    const { verb, score } = evt.detail;

    if (verb === "COMPLETED") {
      const totalScore = state.getScore() + score;
      state.setScore(totalScore);
      const level = state.getLevel();

      if (score >= 80) {
        state.setLevel(level + 1);
        updateLevel(state);
        document.querySelector("dialog > main")!.innerHTML = md2HTML(`Well done.
    
**Level ${level} COMPLETED**.
    
You scored ${score}.

You're total score is ${totalScore}:)
Let's move to next level...`);
      } else {
        document.querySelector("dialog > main").innerHTML = md2HTML(`Sorry.
    
Level ${level} FAILED.
    
You scored ${score} < 80.
    
You're total score is ${totalScore}

<button onClick="window.location.reload();">REPLAY&nbsp;?</button>`);
        updateLevel(state);
        // hide close dialog button
        document.querySelector("#close-dialog")?.classList.add("hidden");
        //   reset state
        state.resetState();
      }
    }
  });
}
